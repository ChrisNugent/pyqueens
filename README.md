# NOTE
Repo is currently very disorganized.

```
usage: nqueens.py [-h] [-p | -q] [-c] size

positional arguments:
  size          Size of board to use

optional arguments:
  -h, --help    show this help message and exit
  -p, --pretty  print board in a visual format
  -q, --quiet   do not print solutions
  -c, --count   display the count with each solution
```